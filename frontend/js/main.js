{
	// Consts used for the fetch process
	const FETCH_INJECT_INTERVAL = 10;
	const FETCH_INJECT_INTERVAL_UNIT = 1000;
	let fetchProcessTimer = null;

	// Query main elements
	const $usersContainer = document.querySelector("#users-feed");
	const $editProfileModal = $("#user-profile-edit");
	const $modal = document.querySelector("#user-profile-edit");

	// Query user info elements, and append event handlers
	const $modalEmail = $modal.querySelector(".user-email");
	$modalEmail.addEventListener("change", function(e) {
		this.user.email = this.value;
	});

	const $modalFullname = $modal.querySelector(".user-full-name");
	$modalFullname.addEventListener("change", function() {
		this.user.fullName = this.value;
	});

	const expandedUserEditHandler = function() {
		$modalEmail.value = this.email;
		$modalEmail.user = this;
		$modalFullname.value = this.fullName;
		$modalFullname.user = this;
		$editProfileModal.modal("show");
	};

	const attachUserInlineEditHandler = function($editable) {
		// Notice the 3rd argument here
		$editable.addEventListener("click", () => ($editable.contentEditable = "true"), true);
		$editable.addEventListener("focusout", () => ($editable.contentEditable = "false"), true);
		$editable.addEventListener(
			"input",
			e => {
				console.log("//TODO: Apply changes to the user's instance");
			},
			true
		);
	};

	const prepareUserContainer = user => {
		const $user = user.toHTML();
		$usersContainer.prepend($user); // Inject user element to dom
		$user.querySelector(".edit-user").addEventListener("click", expandedUserEditHandler.bind(user));
		// Make these elements editable with event handlers
		$user.querySelectorAll(".inline-editable-item").forEach(attachUserInlineEditHandler, user); // The second argument of forEach is context(thisArg)
	};

	window.randomProfiles = Object.freeze({
		usersCache: {},
		startFetch: function continousFetching(numUsers = 10) {
			console.log(`Starting fetch. Total of ${Object.keys(this.usersCache).length} users cached. Fetching ${numUsers} more`);

			this.cancelFetch();

			User.fetchUsers(numUsers)
				.then(users => users.forEach(prepareUserContainer))
				.then(_ => {
					const contextBound = continousFetching.bind(this, numUsers);
					fetchProcessTimer = setTimeout(contextBound, FETCH_INJECT_INTERVAL * FETCH_INJECT_INTERVAL_UNIT);
				})
				.catch(reason => console.log("rendering error", reason.message));
		},
		cancelFetch: function() {
			if (fetchProcessTimer === null) return;
			console.log("Aborting fetch process.");
			clearTimeout(fetchProcessTimer);
			fetchProcessTimer = null;
		},

		getRandom() {
			const usernames = Object.keys(randomProfiles.usersCache);
			const randomIndex = parseInt(Math.random() * usernames.length);

			return randomProfiles.usersCache[usernames[randomIndex]];
		},
		randomlyDelete: function(interval) {
			setInterval(function() {
				randomProfiles.getRandom().$el.classList.add("d-none");
			}, interval);
		}
	});
}
