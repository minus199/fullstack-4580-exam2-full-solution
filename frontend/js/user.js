class User {
	constructor({ gender, name, location, email, login, dob, registered, phone, cell, id, picture, nat }) {
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.location = location;
		this._email = email;
		this.phone = phone;
		this.dob = dob;
		this.picture = picture;

		this.login = login;
		this.registered = registered;
		this.cell = cell;
		this.nat = nat;
		this.$el = null;
	}

	get userName() {
		return this.login.username;
	}

	get fullName() {
		return `${this.name.title} ${this.name.first} ${this.name.last}`;
	}

	set fullName(fullNameString) {
		const [title, first = "", ...last] = fullNameString.split(" ");
		this.name = { title, first, last: last.join(" ") };
		this.toHTML().querySelector(".user-fullname").innerText = this.fullName;
	}

	get email() {
		return this._email;
	}

	set email(newEmail) {
		this._email = newEmail;
		this.toHTML().querySelector(".user-email").innerText = this.email;
	}

	toHTML() {
		if (this.$el === null) {
			this.$el = $userCardTemplate.cloneNode(true);
			this.$el.id = `user-card-${this.login.uuid}`;
			this.$el.querySelector(".user-img").src = this.picture.medium;
			this.$el.querySelector(".user-gender").innerText = this.gender;
			this.$el.querySelector(".user-fullname").innerText = this.fullName;
			this.$el.querySelector(".user-bolking-age").innerText = this.dob.age;
			this.$el.querySelector(".user-email").innerText = this.email;
			this.$el.classList.remove("d-none");
		}

		return this.$el;
	}

	static create(rawUser) {
		const user = window.randomProfiles.usersCache[rawUser.login.username] || new User(rawUser);
		window.randomProfiles.usersCache[user.userName] = user;
		return user;
	}

	// Will only fetch one if no arg has been passed
	static fetchUsers(numUsers) {
		const reqParams = isNaN(numUsers) ? "" : `?results=${numUsers}`;
		return fetch(`${BASE_URL}${reqParams}`)
			.then(res => res.json())
			.then(rawUsers => rawUsers.results.map(this.create))
			.catch(reason => console.log("Error", reason.message));
	}
}
